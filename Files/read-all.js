path = '/home/pc/MEAN/nd-6/Files/readall';
const fs = require("fs");
const conf = {encoding:"utf8"};

function getFiles(path) {
    let fileList = [];
    return  new Promise((done,fail) => {
        fs.readdir(path, (err, files) => {
            if (err) {
                fail(err);
            } else {
                done(files)
            }
        });

    }) 
};

function getContent(files) {
    let fileList = [];
    return new Promise((done, fail) => {
        files.forEach(file => {
            file = '/home/pc/MEAN/nd-6/Files/readall/'+file;
            fs.readFile(file, conf, (err, content) => {
                if (err) {
                    fail(err);
                } else {
                    fileList.push({name:file, content:content});
                     done(fileList);
            
                }
                 
           
        })});
    })
}


function showAll(path) {

getFiles(path)
        .then(data => getContent(data))
        .then(data => console.log(data))
}

module.exports = {
    showAll
}






// fs.readdir(path, (err, files) => {
//     if (err) return console.error(err);
//     let fileList = [];
//     files.forEach(file => { 
//         console.log(file)
//         fileList.push({name:path+'/'+file, content:'empty'});
        
//     });
//     console.log(fileList);
// });
