const EventEmitter = require('events');

class ChatApp extends EventEmitter {
    constructor(title) {
        super();
        this.title = title;
        setInterval(() => {
            this.emit('message', this.title + ': pin-pong');
        }, 1000);
    };
    close() {
        this.emit('close', "Чат " + this.title + " закрылся :(")
    }
}

let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('facebook');
let vkChat = new ChatApp('------vk');

let chatOnMessage = (message) => {
    console.log(message);
};
let prepRespOnMesage = (message) => {
    console.log('Готовлюсь к ответу на сообщение:' + message)
};
let onceOnMesage = () => {
    console.log('Вызывается только один раз, никакие аргументы в обработчик не передаются')
};
let closeApp = (message) => {
    console.log(message);
}


webinarChat
            .once('message', onceOnMesage)
            .on('message', prepRespOnMesage)
            .on('message', chatOnMessage)
            .on('error', console.log);
facebookChat
            .on('message', chatOnMessage)
            .on('error', console.log);
vkChat
            .setMaxListeners(2)
            .on('message', prepRespOnMesage)
            .on('message', chatOnMessage)
            .on('close', closeApp)
            .on('error', console.log);

setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
vkChat.removeListener('message', chatOnMessage);
vkChat.removeListener('message', prepRespOnMesage);
vkChat.close();
}, 10000 );

setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

setTimeout( ()=> {
  console.log('Закрываю вебинар!');
webinarChat.removeListener('message', chatOnMessage);
webinarChat.removeListener('message', prepRespOnMesage);
}, 30000 );





