const fs = require("fs");
const crypto = require("crypto");
var Transform = require('stream').Transform

readFile = 'testfile.js'
writeFile = 'writeFile.js'
let readStream = fs.createReadStream(readFile);
let writeStream = fs.createWriteStream(writeFile);

class CTransform extends Transform{
    constructor (options) {
        super(options);
        this.chunk_count = 0;
    }
    _transform(chunk, encoding, done){
        let hashSum = crypto.createHash('md5').update(chunk).digest('hex');
        this.push(chunk);
        this.chunk_count += 1;
        writeStream.write(hashSum);
        console.log("Chunk count: " + this.chunk_count + " | " + "Hash sum: " + hashSum);
        this.emit('chunk-transformed', chunk, this.chunk_count);
        done();
        
    }
}


let tr = new CTransform();

readStream.on('end', () => {
    console.log('Finished reading file. Closing read stream...');
    readStream.close();
})
writeStream.on('finish', () => {
    console.log('Finished writing file. Closing write stream...');
    writeStream.close();
})
readStream.on('error', (error) => {
    console.log(error);
})
writeStream.on('error', (error) => {
    console.log(error);
})

tr.on('chunk-transformed', (chunk, chunk_count) => {
    console.log('Chunk number: ' + chunk_count + ' has been transformed');
    console.log('Chunk content: ' + chunk);
})

readStream.pipe(tr).pipe(writeStream);



