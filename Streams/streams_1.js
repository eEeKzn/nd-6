const fs = require("fs");
const crypto = require("crypto");

readFile = 'testfile.js'
writeFile = 'writeFile.js'
let readStream = fs.createReadStream(readFile);
let writeStream = fs.createWriteStream(writeFile);
let chunk_count = 0;

readStream.pipe(writeStream);

readStream.on('data', (chunk) => {
    chunk_count += 1;
    let hashSum = crypto.createHash('md5').update(chunk);
    console.log("Chunk count: " + chunk_count + " | " + "Hash sum: " + hashSum);
    console.log('Writing hash sum to file...');
    writeStream.write(hashSum.digest('hex'));
})
readStream.on('end', () => {
    console.log('Finished reading file. Closing read stream...');
    readStream.close();
})
writeStream.on('finish', () => {
    console.log('Finished writing file. Closing write stream...');
    writeStream.close();
})
readStream.on('error', (error) => {
    console.log(error);
})
writeStream.on('error', (error) => {
    console.log(error);
})












